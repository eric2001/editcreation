# EditCreation
![EditCreation](./screenshot.jpg) 

## Description
EditCreation is a module for Gallery 3 which will allow gallery users to manually edit the date/time which an item was created in Gallery on.  

Additional information can be found on the [Gallery 3 Forums](http://gallery.menalto.com/node/90667).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "editcreation" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.  The creation date may then be modified from an album/photo/video's Options->Edit menu.

## History
**Version 1.1.0:**
> - Gallery 3.1.3 compatibility fixes.
> - Released 29 August 2021.
>
> Download: [Version 1.1.0](/uploads/320187360fbd1fa8deab94abe1c0801d/editcreation110.zip)

**Version 1.0.3:**
> - Bug Fix:  Edit fields don't display properly for movies.  Fixed with updated CSS.
> - Tested with Gallery 3.0.3.
> - Released 16 April 2012.
>
> Download: [Version 1.0.3](/uploads/e0dbc7adb1c318dd7b84f886620eaedc/editcreation103.zip)

**Version 1.0.2:**
> - Updated module.info file for Gallery 3.0.2 compatibility.
> - Released 15 August 2011.
>
> Download: [Version 1.0.2](/uploads/c9e1c73c7d6d1b299e6a574153e847a6/editcreation102.zip)

**Version 1.0.1:**
> - Bug Fix for Gallery 3 Beta 3 compatibility.
> - Released 30 September 2009
>
> Download: [Version 1.0.1](/uploads/ee5f06b509d66e3d96991e61b1343bba/editcreation101.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 31 August 2009
>
> Download: [Version 1.0.0](/uploads/891d88df2a78078ae63ebbb04e898f75/editcreation100.zip)

